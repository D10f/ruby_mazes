FROM ruby:3.2.2-slim-bookworm

RUN bundle config --global frozen 1

WORKDIR /usr/src/app

COPY Gemfile* .

RUN bundle install

ARG MAZE_OUTPUT_DIR
ENV MAZE_OUTPUT_DIR=$MAZE_OUT_DIR

ARG MAZE_ALGORITHM
ENV MAZE_ALGORITHM=$MAZE_ALGORITHM

ARG MAZE_TIMESTAMP_FORMAT
ENV MAZE_TIMESTAMP_FORMAT=$MAZE_TIMESTAMP_FORMAT

ARG MAZE_FILE_PREFIX
ENV MAZE_FILE_PREFIX=$MAZE_FILE_PREFIX

COPY . .

CMD ["ruby", "-C", "src", "main.rb"]
