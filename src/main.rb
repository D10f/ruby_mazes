require "./Grid"
require "./DistanceGrid"
require "./BinaryTree"
require "./Sidewinder"

# TODO: Read environment variables to:
#       1.  Determine the output directory for any generated files.
#       2.  Determine the algorithm used to generate the maze.
#       3.  Determine the dimensions of the maze and cell size.
#       4.  Determine the type of output i.e., ascii-based, unicode, png, etc.
#       5.  Determine any file prefixes for generated files i.e., timestamps,
#           checksums, grid size, etc.

MAZE_ALGORITHM = (ENV["MAZE_ALGORITHM"].length > 0) ? ENV["MAZE_ALGORITHM"].upcase : "BINARY_TREE"

TIMESTAMP_FORMAT = (ENV["MAZE_TIMESTAMP_FORMAT"].length > 0) ? ENV["MAZE_TIMESTAMP_FORMAT"] : "%H:%M"

FILE_PREFIX = (ENV["MAZE_FILE_PREFIX"].length > 0) \
  ? ENV["MAZE_FILE_PREFIX"]
  : MAZE_ALGORITHM.downcase << "_" << Time.now.strftime(TIMESTAMP_FORMAT)

OUTPUT_DIR = "/output/"

# Create Grid
# grid = Grid.new(5, 5)

# Configure grid using maze algorithm
# if MAZE_ALGORITHM == "BINARY_TREE"
#   BinaryTree.on(grid)
# elsif MAZE_ALGORITHM == "SIDEWINDER"
#   Sidewinder.on(grid)
# else
#   raise "Unknown algorithm provided."
# end

# Output result to terminal output
# puts grid

# Output result to a file
# img = grid.to_png(cell_size: 50)
# img.save(OUTPUT_DIR << "maze.png")

# ENV.each do |k, v|
#   print(k, "=", v, "\n")
# end

# puts ENV["MAZE_ALGORITHM"]
# puts MAZE_ALGORITHM

grid = DistanceGrid.new(10, 10)
Sidewinder.on(grid)
# start = grid[grid.rows - 1, grid.columns / 2]
# distances = start.distances
# grid.distances = distances.path_to(grid[0, grid.columns / 2])
puts grid
