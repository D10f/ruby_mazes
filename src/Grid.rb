require "chunky_png"
require "./Cell"

class Grid
  attr_reader :rows, :columns
  attr_accessor :grid

  def initialize(rows, columns)
    @rows, @columns = rows, columns
    @grid = create_grid
    configure_grid
  end

  def create_grid
    Array.new(rows) do |row|
      Array.new(columns) do |column|
        Cell.new(row, column)
      end
    end
  end

  def configure_grid
    each_cell do |cell|
      row, col = cell.row, cell.column
      cell.north = self[row - 1, col]
      cell.east = self[row, col + 1]
      cell.south = self[row + 1, col]
      cell.west = self[row, col - 1]
    end
  end

  def [](row, col)
    return nil unless row.between?(0, @rows - 1)
    return nil unless col.between?(0, @columns - 1)
    @grid[row][col]
  end

  def each_row
    @grid.each do |row|
      yield row
    end
  end

  def each_cell
    each_row do |row|
      row.each do |cell|
        yield cell
      end
    end
  end

  def size
    @rows * @columns
  end

  def random_cell
    row = rand(@rows)
    column = rand(@columns)
    self[row, column]
  end

  def contents_of(cell)
    " "
  end

  def to_s
    output = "+" + "---+" * columns + "\n"

    each_row do |row|
      top = "|"
      bottom = "+"

      row.each do |cell|
        cell ||= Cell.new(-1, -1) unless cell
        # body = "   "
        body = " #{contents_of(cell)} "
        east_boundary = (cell.linked?(cell.east) ? " " : "|")
        top << body << east_boundary

        south_boundary = (cell.linked?(cell.south) ? "   " : "---")
        corner = "+"
        bottom << south_boundary << corner
      end

      output << top << "\n"
      output << bottom << "\n"
    end

    output
  end

  def to_png(cell_size: 10)
    img_width = cell_size * columns
    img_height = cell_size * rows

    background = ChunkyPNG::Color::WHITE
    wall = ChunkyPNG::Color::BLACK

    img = ChunkyPNG::Image.new(img_width + 1, img_height + 1, background)

    each_cell do |cell|
      x1 = cell.column * cell_size
      y1 = cell.row * cell_size
      x2 = (cell.column + 1) * cell_size
      y2 = (cell.row + 1) * cell_size

      img.line(x1, y1, x2, y1, wall) unless cell.north
      img.line(x1, y1, x1, y2, wall) unless cell.west
      img.line(x2, y1, x2, y2, wall) unless cell.linked?(cell.east)
      img.line(x1, y2, x2, y2, wall) unless cell.linked?(cell.south)
    end

    img
  end
end
