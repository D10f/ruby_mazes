require "./Distance"

class Cell
  attr_reader :row, :column
  attr_accessor :north, :east, :south, :west

  def initialize(row, column)
    @row, @column = row, column
    @linked = {}
  end

  def print
    puts "Cell at #{row}, #{column}."
  end

  def neighbours
    list = []
    list << north if north
    list << east if east
    list << south if south
    list << west if west
    list
  end

  # Used to keep track of whatever neighbouring cells this cell in linked to.
  # Linked in this context meaning that the surrounding walls are removed.
  # Use the bidirectional parameter for complex grids
  def link(cell, bidirectional = true)
    @linked[cell] = true
    cell.link(self, false) if bidirectional
    self
  end

  def unlink(cell, bidirectional = true)
    @linked.delete(cell)
    cell.unlink(self, false) if bidirectional
    self
  end

  def linked_to
    @linked.keys
  end

  def linked?(cell)
    @linked.key?(cell)
  end

  def distances
    distances = Distance.new(self)
    frontier = [ self ]

    while frontier.any?
      new_frontier = []

      frontier.each do |cell|
        cell.linked_to.each do |neighbor|
          next if distances[neighbor]
          distances[neighbor] = distances[cell] + 1
          new_frontier << neighbor
        end
      end

      frontier = new_frontier
    end

    distances
  end
end
