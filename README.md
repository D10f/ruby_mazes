# Ruby Mazes

This repository is a follow along of the book by Jamis Buck: "[Mazes For Programmers](https://pragprog.com/titles/jbmaze/mazes-for-programmers/)".

For similar repositories written in different programming languages:

-   [C Mazes](https://codeberg.org/D10f/c_mazes)
-   [Lua Mazes](https://codeberg.org/D10f/lua_mazes)

## How To Run

You will need to have Docker installed to run the code from this repository. Clone it to your machine, build Docker image and run it like so:

```
$ docker build -t ruby-mazes .
$ docker run --rm -it ruby-mazes
```

By default, the image will output the generated maze to the terminal in ASCII. You can provide environment variables that control a myriad of options such as the algorithm used to generate the maze, the type of output (acii, unicode, image file), and others.

## Environment Variables

### **MAZE_OUTPUT_DIR**

Default: `/output`

Output location, inside the container, where generated files will be stored. Use this for output images and to change the location when mounting volumes at runtime.

### **MAZE_ALGORITHM**

Default: `BINARY_TREE`

Which algorithm to use to generate the maze. Possible options are: `BINARY_TREE`, `SIDEWINDER`, ...

### **MAZE_FILE_PREFIX**

Default: `<algorithm>_<HH:MM>`

It deterimnes the name to be used for generated files, when applicable i.e.: when creating images of the generated maze. It defaults to whatever algorithm was used, in lowercase, and a timestamp following the format as specified by the environment variable `MAZE_TIMESTAMP_FORMAT` which by default uses a HH:MM string format.

### **MAZE_TIMESTAMP_FORMAT**

Default: `%H:%M`

The timestamp format used by the other variables, such as `MAZE_FILE_PREFIX`. You can overwrite this with any valid string formatter as described in Ruby's documentation for [Formats for Dates and Times](https://docs.ruby-lang.org/en/master/strftime_formatting_rdoc.html#label-Formats+for+Dates+and+Times).

### TODO:

Read process environment variables to:

+ [ ] Determine the output directory for any generated files.
+ [ ] Determine the algorithm used to generate the maze.
+ [ ] Determine the dimensions of the maze and cell size.
+ [ ] Determine the type of output i.e., ascii-based, unicode, png, etc.
+ [ ] Determine any file prefixes for generated files i.e., timestamps, checksums, grid size, etc.
+ [ ] Validate string passed to TIMESTAMP_FORMAT to be a valid string used by Ruby's strftime method.
